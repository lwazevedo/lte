import { Component, OnInit , ElementRef, Renderer2, OnDestroy} from '@angular/core';

@Component({
  selector: 'lte-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private elRef: ElementRef, private rend: Renderer2) { 
    
  }

  ngOnInit() {
    this.rend.addClass(document.body, 'login-page');
  }

  ngOnDestroy() {
    this.rend.removeClass(document.body, 'modal-open');
  }

}
